package com.mervecavdar.common;

public class AppConstants {

    private AppConstants() {

    }

    public static final String TOPIC_NAME = "test_topic_name";

    public static final String GROUP_ID = "test_group_id";

}
